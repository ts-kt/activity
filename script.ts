type employees = Array<number>;

let staffIds: employees = [1, 2, 3];

interface Employee {
    id: number;
    name: string;
    position: string;
    address?: string;
};


function checkEmployment(employee: Employee):string {
    console.log(typeof message);

    staffIds.forEach((id) => {
        console.log("Checking if employee credentials are valid:");
        console.log(id === employee.id);
        if (id === employee.id) {
            message = "Welcome " + employee.name + ". You are clocked in. Enjoy your day!";
        }
    });

    if (typeof message === "undefined"){
        message = "You are not an employee.";
    };

    return message;

};


let manager: Employee = {
    id: 2,
    name: "John",
    position: "CEO"
};

console.log(checkEmployment(manager));